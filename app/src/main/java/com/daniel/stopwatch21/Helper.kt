package com.daniel.stopwatch21

class Helper
{
    companion object
    {
        fun convertToMs(time: CharSequence): Long
        {
            var ms = 0L
            var timeSeparated = time.split(
                ":", ignoreCase = false, limit = 4).toTypedArray()
            //limit does not throw the rest away, it puts it in the last value
            val size = timeSeparated.size - 1
            timeSeparated[size] = timeSeparated[size].replace(":", "")
            val multiplyWith = arrayOf (1, 1000, 60000, 3600000)
            var i = size
            while (i >= 0)
            {
                val temp = timeSeparated[i].toLongOrNull()
                ms += when (temp)
                {
                    null -> 0
                    else -> temp * multiplyWith[size - i]
                }

                //ms += timeSeparated[i].toLong() * multiplyWith[size - i]
                i--
            }
            return ms
        }
    }
}