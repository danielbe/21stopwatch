package com.daniel.stopwatch21

import android.content.Context
import android.media.AudioManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.SystemClock
import android.view.KeyEvent
import android.widget.Button
import android.widget.TextView
import android.widget.EditText

class MainActivity : AppCompatActivity()
{
    //todo
    //anzeige in taskbar oder so
    //in textdatei schreiben die werte
    //inklusive text der mit reingeschrieben wird

    lateinit var buttonStartPause : Button
    lateinit var buttonReset : Button
    lateinit var buttonStartAt : Button
    lateinit var textviewTime : TextView
    lateinit var textviewInfo : TextView
    lateinit var plaintextTag : EditText
    lateinit var plaintextStartAt : EditText
    var startTime = 0L
    var milliseconds = 0L
    var buffer = 0L
    var updateTime = 0L
    var ms = 0
    var s = 0
    var m = 0
    var h = 0
    var isRunning = false
    lateinit var handler : Handler
    //var tf = Textfile(Values.filePath, Values.fileName)

    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        textviewTime = findViewById(R.id.textviewTime)
        //textviewTime.textAlignment = TextView.TEXT_ALIGNMENT_CENTER
        textviewInfo = findViewById(R.id.textviewInfo)

        plaintextTag = findViewById(R.id.plaintextTag)
        plaintextStartAt = findViewById(R.id.plaintextStartAt)

        buttonStartPause = findViewById(R.id.buttonStartPause)
        buttonStartPause.setOnClickListener { decideStartOrPause() }

        buttonReset = findViewById(R.id.buttonReset)
        buttonReset.setOnClickListener { reset() }

        buttonStartAt = findViewById(R.id.buttonStartAt)
        buttonStartAt.setOnClickListener { setStartTime() }

        handler = Handler()
    }

    override fun onSaveInstanceState(outState: Bundle?)
    {
        outState?.putString("time", textviewTime.text.toString())
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle?)
    {
        textviewTime.text = savedInstanceState?.get("time").toString()
        super.onRestoreInstanceState(savedInstanceState)
    }

    private fun decideStartOrPause()
    {
        if (isRunning) pause()
        else start()
    }

    private fun reset()
    {
        if (isRunning) { pause() }
        milliseconds = 0L
        startTime = 0L
        buffer = 0L
        ms =  0
        s = 0
        m = 0
        h = 0
        textviewTime.text = getString(R.string.zeroTime)
        buttonStartAt.isClickable = true
        //adapter.notify

//        val plaintextTagtext = plaintextTag.text.toString()
//        if (!plaintextTagtext.equals(""))
//        {
//            //tf.write(plaintextTagtext)
//        }
    }

    private fun start()
    {
        startTime = SystemClock.uptimeMillis()
        handler.postDelayed(runnable, 0)
        isRunning = true
        buttonStartPause.text = "PAUSE"
        buttonStartAt.isClickable = false
    }

    private fun pause()
    {
        buffer += milliseconds
        handler.removeCallbacks(runnable)
        isRunning = false
        buttonStartPause.text = "START"
    }

    private fun setStartTime()
    {
        buffer = Helper.convertToMs(plaintextStartAt.text)
        textviewInfo.text = buffer.toString()
        setTextviewTimeText()
    }

     private var runnable = object : Runnable
     {
         override fun run() 
         {
             milliseconds = SystemClock.uptimeMillis() - startTime
             setTextviewTimeText()
             handler.postDelayed(this, 0)
         }
     }

    private fun setTextviewTimeText()
    {
        updateTime = buffer + milliseconds

        s = (updateTime / 1000).toInt()
        m = s / 60
        h = m / 60
        s %= 60
        //m -= h * 60
        m %= 60
        ms = (updateTime % 1000).toInt()

        textviewTime.text = getString(R.string.timeFormat, h, m, s, ms)
    }

    //overrides button press
    //volume up and down overriden to change media volume
    override fun dispatchKeyEvent(event: KeyEvent): Boolean
    {
        val keyCode = event.keyCode
        val audioManager = getSystemService(Context.AUDIO_SERVICE)
                as AudioManager
        val volumeInt = when(keyCode)
        {
            KeyEvent.KEYCODE_VOLUME_UP -> AudioManager.ADJUST_RAISE
            KeyEvent.KEYCODE_VOLUME_DOWN -> AudioManager.ADJUST_LOWER
            //do not override different button press
            else -> return super.dispatchKeyEvent(event)
        }
		if (event.action == KeyEvent.ACTION_DOWN) //test
		{
			audioManager.adjustStreamVolume(AudioManager.STREAM_MUSIC,
				volumeInt, AudioManager.FLAG_SHOW_UI)
		}
        return true
    }
}
