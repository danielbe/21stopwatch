package com.daniel.stopwatch21

import java.io.File

class Textfile
{
    private var file : File

    constructor(filePath: File, fileName : String)
    {
        file = File(filePath, fileName)
        if (!file.exists())
            file.writeText("")
    }

    fun write(text : String)
    {
        file.appendText(text)
    }
}