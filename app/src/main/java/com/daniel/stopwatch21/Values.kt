package com.daniel.stopwatch21

import android.os.Build
import android.os.Environment

class Values
{
    companion object {
        val fileName = "times.txt"
        var filePath = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_DOCUMENTS)
        val androidVersion = Build.VERSION.SDK_INT



        fun initializeFilePath()
        {
            if (androidVersion >= Build.VERSION_CODES.KITKAT)
            {
                //deprecated ab irgendwann später
                filePath = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_DOCUMENTS)
            }
            else
            {

            }
        }
    }
}